﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenuScript : MonoBehaviour {

    public GameObject colorPanel;
    public GameObject mainPanel;
    public TextMeshProUGUI coinBalance;

    private static bool gameJustStarted = true;

    [System.Serializable]
    struct SavedData
    {
        public int coinsTotal;
        public List<string> unlockedColors;

        public SavedData(int coinsTotal, List<string> unlockedColors)
        {
            this.coinsTotal = coinsTotal;
            this.unlockedColors = unlockedColors;
        }
    };

    // Use this for initialization
    void Start () {
        colorPanel.SetActive(false);
        mainPanel.SetActive(true);
        if (gameJustStarted)
        {
            LoadFile();
            gameJustStarted = false;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UnlockColorMenu()
    {
        colorPanel.SetActive(true);
        mainPanel.SetActive(false);
        coinBalance.SetText("Available coins: " + GameData.coinsTotal);
        GameObject[] availableColors = GameObject.FindGameObjectsWithTag("Color");
        foreach (GameObject colorAvail in availableColors)
            if (GameData.unlockedColors.Contains(colorAvail.GetComponent<colorData>().color))
            {
                Debug.Log("Found " + colorAvail.GetComponent<colorData>().color);
                colorAvail.GetComponent<colorData>().text.text = "Unlocked";
                Debug.Log("Text is " + colorAvail.GetComponent<colorData>().text.text);
            }

    }

    public void ChooseColor(GameObject color)
    {
        Debug.Log("Text is " + color.GetComponent<colorData>().text.text);
        if (color.GetComponent<colorData>().text.text != "Unlocked")
        {
            if ((GameData.coinsTotal - color.GetComponent<colorData>().coinValue) >= 0)
            {
                GameData.coinsTotal -= color.GetComponent<colorData>().coinValue;
                Debug.Log("Adding color " + color.GetComponent<colorData>().color);
                GameData.unlockedColors.Add(color.GetComponent<colorData>().color);
                color.GetComponent<colorData>().text.text = "Unlocked";
                coinBalance.SetText("Available coins: " + GameData.coinsTotal);
            }
        }
        else
        {
            GameData.chosenColor = color.GetComponent<colorData>().color;
            GameData.ResetData();
            SceneManager.LoadScene("GameScene");
        }

    }


    public void LoadFile()
    {
        string destination = Application.persistentDataPath + "/coins.txt";
        FileStream file;

        if (File.Exists(destination)) file = File.OpenRead(destination);
        else
        {
            Debug.Log("File not found");
            //GameData.coinsTotal = 0;
            GameData.unlockedColors.Add("white");
            return;
        }

        BinaryFormatter bf = new BinaryFormatter();
        SavedData load = (SavedData)bf.Deserialize(file);
        file.Close();
        GameData.coinsTotal = load.coinsTotal;
        GameData.unlockedColors = load.unlockedColors;
        Debug.Log("Got coin: " + GameData.coinsTotal);
    }

    public void SaveFile()
    {
        string destination = Application.persistentDataPath + "/coins.txt";
        FileStream file;

        if (File.Exists(destination)) file = File.OpenWrite(destination);
        else file = File.Create(destination);

        SavedData save = new SavedData(GameData.coinsTotal, GameData.unlockedColors);

        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(file, save);
        file.Close();
    }

    public void ExitGame()
    {
        SaveFile();
        Application.Quit();
    }
}
