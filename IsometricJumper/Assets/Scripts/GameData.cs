﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameData
{
    public static int boardsJumped;
    public static Queue boardsQueue;
    public static int coinsThisTurn;
    public static int coinsTotal = 0;
    public static List<string> unlockedColors = new List<string>();
    public static string chosenColor;

    static GameData()
    {
        ResetData();
    }

    public static void ResetData()
    {
        boardsJumped = 0;
        boardsQueue = new Queue();
        coinsThisTurn = 0;
    }
    
}
