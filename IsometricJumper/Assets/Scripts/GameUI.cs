﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour {

    public TextMeshProUGUI scoreUI;
    public TextMeshProUGUI coinsThisTurnUI;
    public TextMeshProUGUI coinsTotalUI;

    public GameObject panel;

    // Use this for initialization
    void Start () {
        panel.SetActive(false);
        SphereController.EndGame += EndGameScreen;
	}
	
	// Update is called once per frame
	void Update ()
    {
        scoreUI.text = "Score: " + GameData.boardsJumped;
        coinsThisTurnUI.text = "Coins this turn: " + GameData.coinsThisTurn;
        coinsTotalUI.text = "Total coins: " + GameData.coinsTotal;
    }


    void EndGameScreen()
    {
        Debug.Log("Died");
        panel.SetActive(true);
        SphereController.EndGame -= EndGameScreen;
    }

    public void RestartGame()
    {
        GameData.ResetData();
        SceneManager.LoadScene("GameScene");
    }

    public void ToMainMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
