﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pillarscript : MonoBehaviour
{

    public GameObject gameManager;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Player")
        {
            LevelGenerator.Instance.CreateBridge();
            //Debug.Log("Made a bridge");
        }
    }

}
