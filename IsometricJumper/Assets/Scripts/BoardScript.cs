﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardScript : MonoBehaviour {

    public int boardNumber;
    public bool cracked;
    
    // Use this for initialization
	void Awake () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {

    }

    void OnCollisionEnter(Collision col)
    {
        if (cracked)
            if (col.gameObject.name == "Player")
            {
                StartCoroutine(DelayedDestroy(0.2f));
            }
    }

    IEnumerator DelayedDestroy(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }
}
