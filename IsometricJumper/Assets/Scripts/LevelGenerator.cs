﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour {

    private static LevelGenerator instance;

    public static LevelGenerator Instance { get { return instance; } }



    public GameObject firstPillar;
    public GameObject pillar;
    public GameObject coin;
    public GameObject[] boardArray;

    private Vector3 boardTransform;
    private int boardNum = 0;
    
    // Use this for initialization
	/*void Start ()
    {
        boardTransform = new Vector3(firstPillar.transform.position.x, 15, firstPillar.transform.position.z);
        CreateBridge();
	} */

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

        boardTransform = new Vector3(firstPillar.transform.position.x, 15, firstPillar.transform.position.z);
        CreateBridge();
    }

    // Update is called once per frame
    void Update () {
		
	}


    public void CreateBridge()
    {
        int x = 0;
        int z = 0;

        //get random direction, x(1) or z(2) axis
        int randomDirection = Random.Range(1, 3);
        if (randomDirection == 1)
        {
            x = 1;
            z = 0;
        }
        else
        {
            x = 0;
            z = 1;
        }

        //get random number of boards
        int numOfBoards = Random.Range(7, 14);

        for (int i = 0; i < numOfBoards; i++)
        {
            boardTransform = new Vector3(boardTransform.x + 4 * x, 15, boardTransform.z + 4 * z);

            GameObject newBoard = Instantiate(boardArray[Random.Range(0, 2)], boardTransform, Quaternion.Euler(0, 90 * z, 0)); 
            newBoard.GetComponent<BoardScript>().boardNumber = boardNum++;
            GameData.boardsQueue.Enqueue(newBoard);

            //random chance to create coin
            if (Random.Range(0, 4) % 4 == 0)
            {
                Instantiate(coin, new Vector3(boardTransform.x, boardTransform.y + 1.5f, boardTransform.z), Quaternion.Euler(90, 0, 0));
            }
        }

        boardTransform = new Vector3(boardTransform.x + 4 * x, 15, boardTransform.z + 4 * z);
        GameObject newPillar = Instantiate((pillar), new Vector3 (boardTransform.x, firstPillar.transform.position.y, boardTransform.z), Quaternion.identity);
        GameData.boardsQueue.Enqueue(newPillar);
    }
}
