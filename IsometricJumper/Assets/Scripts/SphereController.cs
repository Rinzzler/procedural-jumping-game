﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereController : MonoBehaviour
{
    public delegate void OnDeath();
    public static event OnDeath EndGame;

    public Material redMat, yellowMat, whiteMat;

    public float distanceWhenRegisteringInput;

    Rigidbody rb;

    private enum Action { Hop, Jump, DoubleJump };
    private bool isGrounded = true;
    private int bridgeCompLayerMask = 1 << 8;
    private bool canRegisterInput;
    private bool maxJumped = false;
    private int spacePressed = 0;
    private bool alreadyJumped = false;
    public GameObject boardToJumpTo;
    private int spacePressedWhenGrounded;
    Coroutine jumpCoroutine;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        switch (GameData.chosenColor)
        {
            case "white":
                GetComponent<Renderer>().material = whiteMat;
                break;
            case "red":
                GetComponent<Renderer>().material = redMat;
                break;
            case "yellow":
                GetComponent<Renderer>().material = yellowMat;
                break;
        }
    }

    void FixedUpdate()
    {
        canRegisterInput = Physics.Raycast(gameObject.transform.position, new Vector3(0, -1, 0), gameObject.GetComponent<SphereCollider>().radius + distanceWhenRegisteringInput, bridgeCompLayerMask, QueryTriggerInteraction.Ignore);
    }

    private void Update()
    {
        if (canRegisterInput)
        {
            //Ignore input if already double jumped
            if (spacePressed < 2)
                if (Input.GetKeyDown("space"))
                {
                    Debug.Log("Space");
                    spacePressed++;
                }
            alreadyJumped = false;
        }
        else if (!alreadyJumped)
        {
            if (spacePressedWhenGrounded != spacePressed && !maxJumped)
                TriggerJumpOnEnd();
            ResetInputState();
        }

        if (isGrounded)
            TriggerJumpOnGround();

        //End game condition
        if (transform.position.y < 8)
        {
            Destroy(gameObject);
            if (EndGame != null)
                EndGame();
        }

        Debug.DrawRay(gameObject.transform.position, new Vector3(0, -1, 0) * (gameObject.GetComponent<SphereCollider>().radius + distanceWhenRegisteringInput), Color.red);
    }

    private void TriggerJumpOnGround()
    {
        if (spacePressed != 0)
        {
            TriggerJump(0);
        }
        else Hop();
        isGrounded = false;
        spacePressedWhenGrounded = spacePressed;
    }

    private void TriggerJump(int numOfDequedBoards)
    {
        int i;
        for (i = numOfDequedBoards; i < spacePressed; i++)
        {
            GameData.boardsJumped++;
            if (FindNextBoardToJumpTo() == 1)
            {
                i++;
                maxJumped = true;
                break;
            }
        }

        switch ((Action)i)
        {
            case Action.Jump:
                Jump();
                break;
            case Action.DoubleJump:
                DoubleJump();
                break;
        }
    }



    private void TriggerJumpOnEnd()
    {
        if (spacePressed != 0)
        {
            TriggerJump(spacePressedWhenGrounded);
        }
    }

    void OnCollisionEnter()
    {
        isGrounded = true;
    }


    void Jump()
    {
        gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        rb.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
        jumpCoroutine = StartCoroutine(MoveToPosition(gameObject.transform, new Vector2 (boardToJumpTo.transform.position.x, boardToJumpTo.transform.position.z), 1));
    }

    void DoubleJump()
    {
        gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        rb.AddForce(new Vector3(0, 6, 0), ForceMode.Impulse);
        StopCoroutine(jumpCoroutine);
        StartCoroutine(MoveToPosition(gameObject.transform, new Vector2(boardToJumpTo.transform.position.x, boardToJumpTo.transform.position.z), 1.3f));
    }

    void Hop()
    {
        rb.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
    }

    public IEnumerator MoveToPosition(Transform transform, Vector2 position, float timeToMove)
    {
        var currentPos = new Vector2 (transform.position.x, transform.position.z);
        var t = 0f;
        while (t < 1)
        {
            t += Time.deltaTime / timeToMove;
            var movementOffset = Vector2.Lerp(currentPos, new Vector2 (position.x, position.y), t);
            transform.position = new Vector3 (movementOffset.x, transform.position.y, movementOffset.y);
            yield return new WaitForFixedUpdate();
        }
    }

    void ResetInputState()
    {
        spacePressed = 0;
        maxJumped = false;
        alreadyJumped = true;
        spacePressedWhenGrounded = 0;
    }

    int FindNextBoardToJumpTo()
    {
        boardToJumpTo = GameData.boardsQueue.Dequeue() as GameObject;
        if (boardToJumpTo.tag == "Pillar")
            return 1;
        return 0;
    }
}
